function [p_opt,r_opt,alpha_opt,R_opt,feasible_SIC]=...
    func_alg_PA_sumrate(B,U,M,R_min,P_max,ceil_amin,ICI_indctr,h,h_cell,N0,eps_a,SIC_CNR)
%Initialization
alpha=1*ones(1,B);
p_opt=0;
r_opt=0;
alpha_opt=0;
R_opt=0;
feasible_SIC=0;
for b=1:B
    for i=1:U(b)
        R_minextra(b,i)=R_min(b,i)+0.0000001;
    end
end
while alpha(1)>ceil_amin(1)
    alpha(2)=1;
    while alpha(2)>ceil_amin(2)
        ICI=reshape(sum(repmat(alpha'.*P_max',1,B,M).*ICI_indctr.*h,1),B,M);
        h_norm=h_cell./(ICI+N0);
        [SIC_order]=func_SICordering(B,U,M,h_norm);
        if (min(SIC_order==SIC_CNR,[],'all')==1)
            feasible_SIC=1;
            [p]=func_optpower_sumrate(B,U,M,h_norm,R_minextra,SIC_order,P_max,alpha);
            [r]=func_rate(B,U,M,SIC_order,p,h_norm);
            if ( min(min(r>=R_min))==1 && sum(r,'all') > R_opt)
                R_opt=sum(r,'all');
                p_opt=p;
                r_opt=r;
                alpha_opt=alpha;
            end
        end
        %Update (reduce) alpha(2)
        alpha(2)=alpha(2)-eps_a;
    end
    %Update (reduce) alpha(1)
    alpha(1)=alpha(1)-eps_a;
end
