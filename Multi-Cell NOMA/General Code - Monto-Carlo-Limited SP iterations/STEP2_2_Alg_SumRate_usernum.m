%% Performance Evaluation-Different Order of NOMA Clusters
clear all
clc
load('STEP1_Settings_MultiCell','B','AWGN_sample','h_sample','h_cell_sample','num_Mcell_user',...
    'num_Fcell_user','Distance_BS','PSD_noise','max_samp')
load('STEP2_1_Alg_Powmin_usernum','P_max','rate_min_m','rate_min_f',...
    'SIC_order_CNR_sample','lambda_CNR_sample',...
    'Feasible_JSPA_sample','alpha_JSPA_PM_sample','p_JSPA_PM_sample','SIC_order_JSPA_PM_sample',...
    'Feasible_JRPA_sample','alpha_JRPA_PM_sample','p_JRPA_PM_sample',...
    'Feasible_FRPA_sample','alpha_FRPA_PM_sample','p_FRPA_PM_sample')
m=(log(2.^30 - 1) - log(2.^(29.999) - 1))/0.001; %approx term log(2.^r - 1);
eps_a=1e-2; %stepsize alpha
eps_s=1e-1; %stopping criterion for sequential program

fprintf('***Total Samples***\n\n')
tot_samplss=(length(1:length(num_Mcell_user)))*length(num_Fcell_user)*max_samp*length(rate_min_m)*length(rate_min_f);
disp(tot_samplss)
X = sprintf('---Please wait about %d hours!\n\n',round(tot_samplss/(0.2*3600),3));...
disp(X)

tt=0; %samples iteration index
for clstr_m=1:1:length(num_Mcell_user)
    for clstr_f=1:length(num_Fcell_user)
        for sampl=1:max_samp
            h=[];
            h=h_sample{clstr_m,clstr_f,sampl};
            h_cell=[];
            h_cell=h_cell_sample{clstr_m,clstr_f,sampl};
            N0=[];
            N0=AWGN_sample{clstr_m,clstr_f,sampl};
            Macro_cell=num_Mcell_user(clstr_m);
            Femto_cell=num_Fcell_user(clstr_f);
            U=[];
            U=[Macro_cell,Femto_cell*ones(1,B-1)];
            M=max(U);
            for r_m=1:length(rate_min_m)
                for r_f=1:length(rate_min_f)
                    %%Minimum spectral efficiency
                    R_macro=rate_min_m(r_m);%bps/Hz
                    R_femto=rate_min_f(r_f);%bps/Hz
                    R_min=[];
                    [R_min]=func_minrate(B,U,M,R_macro,R_femto);
                    ICI_indctr=[];
                    ICI_indctr=func_ICI_indicator(B,M);
                    R_minextra=[];
                    for b=1:B
                        for i=1:U(b)
                            R_minextra(b,i)=R_min(b,i)+0.0001;
                        end
                    end
%% CNR-based Decoding Order
SIC_CNR=SIC_order_CNR_sample{clstr_m,clstr_f,r_m,r_f,sampl};
lambda_CNR=lambda_CNR_sample{clstr_m,clstr_f,r_m,r_f,sampl};

%% Sum-rate maximization problem: JSPA & FRPA with CNR-Based Decoding Order
%JSPA initialization
p_JSPA_SM_sample{clstr_m,clstr_f,r_m,r_f,sampl}=...
    Feasible_JSPA_sample(clstr_m,clstr_f,r_m,r_f,sampl)*...
    p_JSPA_PM_sample{clstr_m,clstr_f,r_m,r_f,sampl};
alpha_JSPA_SM_sample{clstr_m,clstr_f,r_m,r_f,sampl}=...
    Feasible_JSPA_sample(clstr_m,clstr_f,r_m,r_f,sampl)*...
    alpha_JSPA_PM_sample{clstr_m,clstr_f,r_m,r_f,sampl};
SIC_order_JSPA_SM_sample{clstr_m,clstr_f,r_m,r_f,sampl}=...
    Feasible_JSPA_sample(clstr_m,clstr_f,r_m,r_f,sampl)*...
    SIC_order_JSPA_PM_sample{clstr_m,clstr_f,r_m,r_f,sampl};
R_user_JSPA_SM_sample{clstr_m,clstr_f,r_m,r_f,sampl}=...
    Feasible_JSPA_sample(clstr_m,clstr_f,r_m,r_f,sampl)*R_min;
R_tot_JSPA_SM_sample(clstr_m,clstr_f,r_m,r_f,sampl)=...
    sum(R_user_JSPA_SM_sample{clstr_m,clstr_f,r_m,r_f,sampl},'all');
NonEmpty_JSPA_SM_sample(clstr_m,clstr_f,r_m,r_f,sampl)=1;

%FRPA initialization
p_FRPA_SM_sample{clstr_m,clstr_f,r_m,r_f,sampl}=...
    Feasible_FRPA_sample(clstr_m,clstr_f,r_m,r_f,sampl)*...
    p_FRPA_PM_sample{clstr_m,clstr_f,r_m,r_f,sampl};
alpha_FRPA_SM_sample{clstr_m,clstr_f,r_m,r_f,sampl}=...
    Feasible_FRPA_sample(clstr_m,clstr_f,r_m,r_f,sampl)*...
    alpha_FRPA_PM_sample{clstr_m,clstr_f,r_m,r_f,sampl};
R_user_FRPA_SM_sample{clstr_m,clstr_f,r_m,r_f,sampl}=...
    Feasible_FRPA_sample(clstr_m,clstr_f,r_m,r_f,sampl)*R_min;
R_tot_FRPA_SM_sample(clstr_m,clstr_f,r_m,r_f,sampl)=...
    sum(R_user_FRPA_SM_sample{clstr_m,clstr_f,r_m,r_f,sampl},'all');
NonEmpty_FRPA_SM_sample(clstr_m,clstr_f,r_m,r_f,sampl)=1;

%Combined Algorithm
if (Feasible_JSPA_sample(clstr_m,clstr_f,r_m,r_f,sampl)==1)
    NonEmpty_JSPA_SM=0;
    NonEmpty_FRPA_SM=1-Feasible_FRPA_sample(clstr_m,clstr_f,r_m,r_f,sampl);
    %Reducing the exploration area of alpha
    alpha_min=alpha_JSPA_PM_sample{clstr_m,clstr_f,r_m,r_f,sampl}; 
    ceil_amin=ceil(alpha_min*ceil(inv(eps_a)))./inv(eps_a); %vector of minimum alpha vector
    %initialize
    alpha=1*ones(1,B); %for the while loop
    %initialization optimal JSPA
    p_JSPA_SM=p_JSPA_SM_sample{clstr_m,clstr_f,r_m,r_f,sampl};
    alpha_JSPA_SM=alpha_JSPA_SM_sample{clstr_m,clstr_f,r_m,r_f,sampl};
    SIC_order_JSPA_SM=SIC_order_JSPA_SM_sample{clstr_m,clstr_f,r_m,r_f,sampl};
    R_user_JSPA_SM=R_user_JSPA_SM_sample{clstr_m,clstr_f,r_m,r_f,sampl};
    R_tot_JSPA_SM=R_tot_JSPA_SM_sample(clstr_m,clstr_f,r_m,r_f,sampl);
    %initialization optimal FRPA
    p_FRPA_SM=p_FRPA_SM_sample{clstr_m,clstr_f,r_m,r_f,sampl};
    alpha_FRPA_SM=alpha_FRPA_SM_sample{clstr_m,clstr_f,r_m,r_f,sampl};
    R_user_FRPA_SM=R_user_FRPA_SM_sample{clstr_m,clstr_f,r_m,r_f,sampl};
    R_tot_FRPA_SM=R_tot_FRPA_SM_sample(clstr_m,clstr_f,r_m,r_f,sampl);
    while alpha(1)>ceil_amin(1)
        alpha(2)=1;
        while alpha(2)>ceil_amin(2)
            ICI=reshape(sum(repmat(alpha'.*P_max',1,B,M).*ICI_indctr.*h,1),B,M);
            h_norm=h_cell./(ICI+N0);    
            [SIC_order]=func_SICordering(B,U,M,h_norm);
            [p]=func_optpower_sumrate(B,U,M,h_norm,R_minextra,SIC_order,P_max,alpha);
            [r]=func_rate(B,U,M,SIC_order,p,h_norm);
            %For JSPA
            if ( min(r>=R_min,[],'all')==1 && sum(r,'all') > R_tot_JSPA_SM)
                NonEmpty_JSPA_SM=1;
                p_JSPA_SM=p;
                alpha_JSPA_SM=alpha;
                SIC_order_JSPA_SM=SIC_order;
                R_user_JSPA_SM=r;
                R_tot_JSPA_SM=sum(r,'all');
            end
            %For FRPA
            if (min(SIC_order==SIC_CNR,[],'all')==1)
                if ( min(r>=R_min,[],'all')==1 && sum(r,'all') > R_tot_FRPA_SM)
                    NonEmpty_FRPA_SM=1;
                    p_FRPA_SM=p;
                    alpha_FRPA_SM=alpha;
                    R_user_FRPA_SM=r;
                    R_tot_FRPA_SM=sum(r,'all');
                end
            end
            %Update (reduce) alpha(2)
            alpha(2)=alpha(2)-eps_a;
        end
        %Update (reduce) alpha(1)
        alpha(1)=alpha(1)-eps_a;
    end
    %SAVE JSPA
    NonEmpty_JSPA_SM_sample(clstr_m,clstr_f,r_m,r_f,sampl)=NonEmpty_JSPA_SM;
    p_JSPA_SM_sample{clstr_m,clstr_f,r_m,r_f,sampl}=p_JSPA_SM;
    alpha_JSPA_SM_sample{clstr_m,clstr_f,r_m,r_f,sampl}=alpha_JSPA_SM;
    SIC_order_JSPA_SM_sample{clstr_m,clstr_f,r_m,r_f,sampl}=SIC_order_JSPA_SM;
    R_user_JSPA_SM_sample{clstr_m,clstr_f,r_m,r_f,sampl}=R_user_JSPA_SM;
    R_tot_JSPA_SM_sample(clstr_m,clstr_f,r_m,r_f,sampl)=R_tot_JSPA_SM;
    %SAVE FRPA
    NonEmpty_FRPA_SM_sample(clstr_m,clstr_f,r_m,r_f,sampl)=NonEmpty_FRPA_SM;
    p_FRPA_SM_sample{clstr_m,clstr_f,r_m,r_f,sampl}=p_FRPA_SM;
    alpha_FRPA_SM_sample{clstr_m,clstr_f,r_m,r_f,sampl}=alpha_FRPA_SM;
    R_user_FRPA_SM_sample{clstr_m,clstr_f,r_m,r_f,sampl}=R_user_FRPA_SM;
    R_tot_FRPA_SM_sample(clstr_m,clstr_f,r_m,r_f,sampl)=R_tot_FRPA_SM;
    
    %Approximated Closed form of opt powers
    if (NonEmpty_JSPA_SM==1)
        SIC_order=SIC_order_JSPA_SM;
        [q]=func_approx_sumratemax(B,U,M,SIC_order,R_min);
        p=q.*repmat((alpha_JSPA_SM.*P_max)',1,M)/100;
        ICI=reshape(sum(repmat(alpha_JSPA_SM'.*P_max',1,B,M).*ICI_indctr.*h,1),B,M);
        h_norm=h_cell./(ICI+N0);
        [r]=func_rate(B,U,M,SIC_order,p,h_norm);
        %SAVE
        p_approx_JSPA_SM_sample{clstr_m,clstr_f,r_m,r_f,sampl}=p;
        R_approx_user_JSPA_SM_sample{clstr_m,clstr_f,r_m,r_f,sampl}=r;
        R_approx_tot_JSPA_SM_sample(clstr_m,clstr_f,r_m,r_f,sampl)=sum(r,'all');
    end
end

%% Sum-rate Maximization Problem: CNR-Based Decoding Order with Suboptimal JRPA
%initialization
p_JRPA_SM_sample{clstr_m,clstr_f,r_m,r_f,sampl}=...
    Feasible_JRPA_sample(clstr_m,clstr_f,r_m,r_f,sampl)*...
    p_JRPA_PM_sample{clstr_m,clstr_f,r_m,r_f,sampl};
alpha_JRPA_SM_sample{clstr_m,clstr_f,r_m,r_f,sampl}=...
    Feasible_JRPA_sample(clstr_m,clstr_f,r_m,r_f,sampl)*...
    alpha_JRPA_PM_sample{clstr_m,clstr_f,r_m,r_f,sampl};
R_user_JRPA_SM_sample{clstr_m,clstr_f,r_m,r_f,sampl}=...
    Feasible_JRPA_sample(clstr_m,clstr_f,r_m,r_f,sampl)*R_min;
R_tot_JRPA_SM_sample(clstr_m,clstr_f,r_m,r_f,sampl)=...
    sum(R_user_JRPA_SM_sample{clstr_m,clstr_f,r_m,r_f,sampl},'all');
Failed_JRPA_SM_sample(clstr_m,clstr_f,r_m,r_f,sampl)=0;
if (Feasible_JRPA_sample(clstr_m,clstr_f,r_m,r_f,sampl)==1)
    if min(SIC_order_JSPA_SM==SIC_CNR,[],'all')==1
        p_JRPA_SM_sample{clstr_m,clstr_f,r_m,r_f,sampl}=...
            p_JSPA_SM_sample{clstr_m,clstr_f,r_m,r_f,sampl};
        alpha_JRPA_SM_sample{clstr_m,clstr_f,r_m,r_f,sampl}=...
            alpha_JSPA_SM_sample{clstr_m,clstr_f,r_m,r_f,sampl};
        R_user_JRPA_SM_sample{clstr_m,clstr_f,r_m,r_f,sampl}=...
            R_user_JSPA_SM_sample{clstr_m,clstr_f,r_m,r_f,sampl};
        R_tot_JRPA_SM_sample(clstr_m,clstr_f,r_m,r_f,sampl)=...
            R_tot_JSPA_SM_sample(clstr_m,clstr_f,r_m,r_f,sampl);
    else
        %Initialization step
        SIC_order=SIC_CNR; %SIC ordering based on CNR
        lambda=lambda_CNR; %cancellation decision indicator
        p=[]; p_tilde=[]; r=[]; ICI=[];
        t=1; %iteration index
        r=R_min; p=p_JRPA_PM_sample{clstr_m,clstr_f,r_m,r_f,sampl}; %%%DIFFERENT FROM UPLOADING...
        p_tilde=log(p_JRPA_PM_sample{clstr_m,clstr_f,r_m,r_f,sampl}); ICI=[]; %%%DIFFERENT FROM UPLOADING...
        R_user_iter_JRPA=[];
        R_user_iter_JRPA{t}=r;
        R_tot_iter_JRPA(t)=sum(r,'all');
        p_iter_JRPA=[];
        p_iter_JRPA{t}=exp(p_tilde);
        % Sequential Programming
        r_old=0;
        SCA_it=0; %%%DIFFERENT FROM UPLOADING...
        while (SCA_it<4) %%%DIFFERENT FROM UPLOADING...
            SCA_it=SCA_it+1; %%%DIFFERENT FROM UPLOADING...
            t=t+1;
            r_old=r;    p_tilde_old=p_tilde;
            %CVX
            r=[];   p=[];   p_tilde=[]; ICI=[];
            cvx_begin quiet
            cvx_precision low %%%DIFFERENT FROM UPLOADING...
                variable p_tilde(B,M)
                variable r(B,M)
                subject to
                %power constraint
                sum(exp(p_tilde),2)<=P_max';
                %Min rate constraint (main)
                ICI=reshape(sum(repmat(sum(exp(p_tilde),2),1,B,M).*ICI_indctr.*h,1),B,M); %Inter-cell Interference
                for b=1:B
                    for i=1:M
                        if i<=U(b)
                            1e2*(p_tilde(b,i) + log(h_cell(b,i))) >= 1e2*( log(2^r_old(b,i)-1) + ...
                                (2^r_old(b,i)/(2^r_old(b,i)-1))*(r(b,i)-r_old(b,i)) + ...
                                log( sum(reshape(lambda(b,i,:),1,M).* exp(p_tilde(b,:)) ) .* h_cell(b,i) ...
                                + ICI(b,i) + N0(b,i)));
                        else r(b,i)==0
                        end
                    end
                end
                %SIC constraint
                for b=1:B
                    for i=1:U(b)
                        for k=1:U(b)
                            if lambda(b,i,k)==1
                                1e2*(p_tilde(b,i) + log(h_cell(b,k))) >= 1e2*( log(2.^r_old(b,i)-1) + ...
                                ((2.^r_old(b,i))/(2.^r_old(b,i)-1))*(r(b,i)-r_old(b,i)) + ...
                                log( sum(reshape(lambda(b,i,:),1,M).* exp(p_tilde(b,:)) ) .* h_cell(b,k) ...
                                + ICI(b,k) + N0(b,k)));
                            end
                        end
                    end
                end
                %minimum rate
                r>=R_min;
                %Positive power
                r>=0;
                maximize sum(sum(r))   
            cvx_end
            if min(isfinite(r),[],'all')==0
                Failed_JRPA_SM_sample(clstr_m,clstr_f,r_m,r_f,sampl)=1;
                r=r_old;    p_tilde=p_tilde_old;    p=exp(p_tilde_old);   ICI=[];
                break
            end
            R_user_iter_JRPA{t}=r;
            R_tot_iter_JRPA(t)=sum(r,'all');
            p_iter_JRPA{t}=exp(p_tilde);
        end
        p_JRPA_SM_sample{clstr_m,clstr_f,r_m,r_f,sampl}=exp(p_tilde);
        alpha_JRPA_SM_sample{clstr_m,clstr_f,r_m,r_f,sampl}=sum(exp(p_tilde),2)'./P_max;
        R_user_JRPA_SM_sample{clstr_m,clstr_f,r_m,r_f,sampl}=r;
        R_tot_JRPA_SM_sample(clstr_m,clstr_f,r_m,r_f,sampl)=sum(r,'all');
        %clearing...
        p_tilde=[]; ICI=[]; r=[];
    end
end

%% Sum-rate Maximization Problem: Fully Distributed Framework
R_DF_sample(clstr_m,clstr_f,r_m,r_f,sampl)=0;
Feasible_DF_sample(clstr_m,clstr_f,r_m,r_f,sampl)=0;
SIC_order_DF_sample{clstr_m,clstr_f,r_m,r_f,sampl}=[];
if (Feasible_JSPA_sample(clstr_m,clstr_f,r_m,r_f,sampl)==1)
    alpha=ones(1,B);
    ICI=reshape(sum(repmat(alpha'.*P_max',1,B,M).*ICI_indctr.*h,1),B,M);
    h_norm=h_cell./(ICI+N0);
    [SIC_order]=func_SICordering(B,U,M,h_norm);
    SIC_order_DF_sample{clstr_m,clstr_f,r_m,r_f,sampl}=SIC_order;
    [p]=func_optpower_sumrate(B,U,M,h_norm,R_minextra,SIC_order,P_max,alpha);
    [r]=func_rate(B,U,M,SIC_order,p,h_norm);
    if (min(r>=R_min,[],'all')==1)
        R_DF_sample(clstr_m,clstr_f,r_m,r_f,sampl)=sum(r,'all');
        Feasible_DF_sample(clstr_m,clstr_f,r_m,r_f,sampl)=1;
    end
    p=[]; r=[];
end

%% Sum-rate Maximization Problem: Semi-Centralized Algorithm
Rtot_SC_sumrate_sample(clstr_m,clstr_f,r_m,r_f,sampl)=0;
alpha_SC_sumrate_sample{clstr_m,clstr_f,r_m,r_f,sampl}=[];
r_SC_sumrate_sample{clstr_m,clstr_f,r_m,r_f,sampl}=[];
p_SC_sumrate_sample{clstr_m,clstr_f,r_m,r_f,sampl}=[];
Feasible_SC_sample(clstr_m,clstr_f,r_m,r_f,sampl)=0;
SIC_order_SC_sample{clstr_m,clstr_f,r_m,r_f,sampl}=[];
if (Feasible_JSPA_sample(clstr_m,clstr_f,r_m,r_f,sampl)==1)
    [p_SC,r_SC,alpha_SC,SIC_order_SC,Rtot_SC,Feasible_SC]=...
        func_alg_SC_sumrate(B,U,M,R_min,P_max,ceil_amin,ICI_indctr,h,h_cell,N0,eps_a);
    Feasible_SC_sample(clstr_m,clstr_f,r_m,r_f,sampl)=Feasible_SC;
    %%%%%Results
    if (Feasible_SC==1)
        Rtot_SC_sumrate_sample(clstr_m,clstr_f,r_m,r_f,sampl)=Rtot_SC;
        alpha_SC_sumrate_sample{clstr_m,clstr_f,r_m,r_f,sampl}=alpha_SC;
        r_SC_sumrate_sample{clstr_m,clstr_f,r_m,r_f,sampl}=r_SC;
        p_SC_sumrate_sample{clstr_m,clstr_f,r_m,r_f,sampl}=p_SC;
        SIC_order_SC_sample{clstr_m,clstr_f,r_m,r_f,sampl}=SIC_order_SC;
    end
end

%%
tt=tt+1
% if (tt/tot_samplss)==0.25
%     fprintf('*** 25 percentage of samples are calculated! ***\n\n')
% elseif (tt/tot_samplss)==0.5
%     fprintf('*** 50 percentage of samples are calculated! ***\n\n')
% elseif (tt/tot_samplss)==0.75
%     fprintf('*** 75 percentage of samples are calculated! ***\n\n')
% end
                end
            end
        end
    end
end

fprintf('*** Finished ! ***\n\n')

save('STEP2_2_Alg_SumRate_usernum','P_max','rate_min_m','rate_min_f','m','eps_a','eps_s',...
    'p_JSPA_SM_sample','alpha_JSPA_SM_sample','SIC_order_JSPA_SM_sample',...
    'R_user_JSPA_SM_sample','R_tot_JSPA_SM_sample','NonEmpty_JSPA_SM_sample',...
    'p_FRPA_SM_sample','alpha_FRPA_SM_sample','R_user_FRPA_SM_sample',...
    'R_tot_FRPA_SM_sample','NonEmpty_FRPA_SM_sample','p_approx_JSPA_SM_sample',...
    'R_approx_user_JSPA_SM_sample','R_approx_tot_JSPA_SM_sample',...
    'p_JRPA_SM_sample','alpha_JRPA_SM_sample','R_user_JRPA_SM_sample',...
    'R_tot_JRPA_SM_sample','Failed_JRPA_SM_sample','R_DF_sample','Feasible_DF_sample',...
    'SIC_order_DF_sample','p_SC_sumrate_sample','alpha_SC_sumrate_sample',...
    'r_SC_sumrate_sample','SIC_order_SC_sample','Rtot_SC_sumrate_sample','Feasible_SC_sample')
