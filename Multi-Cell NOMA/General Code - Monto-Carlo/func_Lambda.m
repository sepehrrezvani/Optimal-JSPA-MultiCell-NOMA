function [lambda] = func_Lambda(B,U,M,SIC_order)
lambda=zeros(B,M,M);
for b=1:B
    for m=1:U(b)
        for m_prn=1:U(b)
            if(SIC_order(b,m) < SIC_order(b,m_prn))
                lambda(b,m,m_prn)=1;
            end
        end
    end
end
