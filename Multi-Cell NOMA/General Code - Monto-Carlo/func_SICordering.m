function [SIC_order]=func_SICordering(B,U,M,h_norm)
SIC_order=zeros(B,M); %decoding order based on updated normalized channel gains, 
%higher value corresponds to the higher decoding order
for b=1:B
    count=0;
    while(count <= U(b)-1)
        stronguser=find(h_norm(b,:)==max(h_norm(b,:)));
        SIC_order(b,stronguser)=U(b)-count;
        count=count+1;
        h_norm(b,stronguser)=0;
    end
end
