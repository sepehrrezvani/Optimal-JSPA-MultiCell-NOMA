-The results are available on

https://cloudstorage.tu-braunschweig.de/getlink/fiPL9w69hssZLHCrUxBd8Qn9/Results_General_Code_Monto-Carlo_10_PCs.zip

-Please download the zip file. Then, extract it and copy the files to the directory "Plot Figure-10000 Samples-10 PCs".
-After that, run "STEP3_PlotFigure.m".
-The plotted Figures can be downloaded in the following address:

https://cloudstorage.tu-braunschweig.de/getlink/fiUmtB1WmjGRzBMEv52im7rn/MATLAB_Figures.zip