function [alpha,p,P_tot_iter,iteration_num,SIC_order,h_norm,Feasible_JSPA,diverges]=...
    func_alg_minimize_pow(B,U,M,h,h_cell,N0,R_min,P_max,ICI_indctr,eps_tol,alpha_initial)
p=[]; P_tot_iter=[]; SIC_order=[];
%Initialization Step
alpha=alpha_initial;
ICI=reshape(sum(repmat(alpha_initial'.*P_max',1,B,M).*ICI_indctr.*h,1),B,M);
h_norm=h_cell./(ICI+N0);
[SIC_order]=func_SICordering(B,U,M,h_norm);
[p]=func_opt_powermin(B,U,M,SIC_order,h_norm,R_min);
p_old=sum(P_max)*ones(B,M); %Set to a large number
diverges=1;
iteration_num=1; %initialization step
P_tot_iter(iteration_num,:)=sum(p,'all')*zeros(1,B);
%Algorithm
while (norm(p_old-p) > eps_tol)
    for b=1:B
        p_old=p;
        h_norm(b,:)=h_cell(b,:)./(ICI(b,:)+N0(b,:));
        [SIC_order_cell]=func_SICordering_cell(b,U,M,h_norm);
        SIC_order(b,:)=SIC_order_cell;
        [p_cell]=func_opt_powermin_cell(b,U,M,SIC_order,h_norm,R_min);
        p(b,:)=p_cell;
        alpha(1,b)=sum(p(b,:))./P_max(1,b);
        ICI=reshape(sum(repmat(alpha'.*P_max',1,B,M).*ICI_indctr.*h,1),B,M);
        %divergence
        if (sum(p,'all')>1e+10)
            diverges=1;
            break
        else diverges=0;
        end
        P_tot_iter(iteration_num,b)=sum(p,'all');
    end
    iteration_num=iteration_num+1;
end
Feasible_JSPA =(max(alpha)<=1);




