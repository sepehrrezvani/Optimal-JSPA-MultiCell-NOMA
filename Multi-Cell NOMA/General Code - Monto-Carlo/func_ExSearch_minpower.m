function [Feasibility_greed]=func_ExSearch_minpower(B,U,M,R_min,P_max,ICI_indctr,h,h_cell,N0,eps_a,alpha_initial)
for b=1:B
    for i=1:U(b)
        R_minextra(b,i)=R_min(b,i)+0.0000001;
    end
end
%Initialization
alpha=ones(1,B);
Feasibility_greed=0;
while alpha(1)>0
    alpha(2)=alpha_initial(2);
    while alpha(2)>0
        ICI=reshape(sum(repmat(alpha'.*P_max',1,B,M).*ICI_indctr.*h,1),B,M);
        h_norm=h_cell./(ICI+N0);
        [SIC_order]=func_SICordering(B,U,M,h_norm);
        [p]=func_optpower_sumrate(B,U,M,h_norm,R_minextra,SIC_order,P_max,alpha);
        [r]=func_rate(B,U,M,SIC_order,p,h_norm);
        Feasibility_greed=min(min((r>=R_min)));
        %feasible solution found, so break the iterations
        if (Feasibility_greed==1)
            break
        end
        %Update (reduce) alpha(2)
        alpha(2)=alpha(2)-eps_a;
    end
    if (Feasibility_greed==1) %feasible solution found, so break the iterations
        break
    end
    %Update (reduce) alpha(1)
    alpha(1)=alpha(1)-eps_a;
end
