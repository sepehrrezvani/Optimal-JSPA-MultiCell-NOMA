%% Performance Evaluation-Different Order of NOMA Clusters
clear all
clc
load('STEP1_Settings_MultiCell','B','AWGN_sample','h_sample','h_cell_sample','num_Mcell_user',...
    'num_Fcell_user','Distance_BS','PSD_noise','max_samp')
tt=0; %samples iteration index
%%%%%Transmit power of BSs
P_MBS=46; %MBS power in dbm
P_FBS=30; %MBS power in dbm
P_max=[P_MBS,P_FBS*ones(1,B-1)]; %BSs power (in dbm)
P_max=10.^((P_max-30)./10); %BSs power in Watts
%%Minimum rate vector (samples)
rate_min_m=[1]; %bps/Hz
rate_min_f=[1]; %bps/Hz
%%stopping criterion for optimal iterative JSPA
eps_tol=1e-6;

fprintf('***Total Samples***\n\n')
tot_samplss=(length(1:length(num_Mcell_user)))*length(num_Fcell_user)*max_samp*length(rate_min_m)*length(rate_min_f);
disp(tot_samplss)
X = sprintf('---Please wait about %d hours!\n\n',round(tot_samplss/(6.2*3600),3));...
disp(X)

for clstr_m=1:1:length(num_Mcell_user)
    for clstr_f=1:length(num_Fcell_user)
        for sampl=1:max_samp
            h=[];
            h=h_sample{clstr_m,clstr_f,sampl};
            h_cell=[];
            h_cell=h_cell_sample{clstr_m,clstr_f,sampl};
            N0=[];
            N0=AWGN_sample{clstr_m,clstr_f,sampl};
            Macro_cell=num_Mcell_user(clstr_m);
            Femto_cell=num_Fcell_user(clstr_f);
            U=[];
            U=[Macro_cell,Femto_cell*ones(1,B-1)];
            M=max(U);
            for r_m=1:length(rate_min_m)
                for r_f=1:length(rate_min_f)
                    %%Minimum spectral efficiency
                    R_macro=rate_min_m(r_m);%bps/Hz
                    R_femto=rate_min_f(r_f);%bps/Hz
                    R_min=[];
                    [R_min]=func_minrate(B,U,M,R_macro,R_femto);
                    ICI_indctr=[];
                    ICI_indctr=func_ICI_indicator(B,M);
                            
%% CNR-based Decoding Order
ICI=[];
h_norm=[];
h_norm=h_cell./N0;
[SIC_order]=func_SICordering(B,U,M,h_norm);
SIC_CNR=SIC_order; %CNR based SIC ordering
[lambda]=func_Lambda(B,U,M,SIC_order);%cancellation decision indicator
lambda_CNR=lambda;
SIC_order_CNR_sample{clstr_m,clstr_f,r_m,r_f,sampl}=SIC_CNR;
lambda_CNR_sample{clstr_m,clstr_f,r_m,r_f,sampl}=lambda_CNR;

%% Total Power Minimization Problem: Optimal joint power allocation and SIC ordering (JSPA)
num_inc_pair_powmin_MBS_sample(clstr_m,clstr_f,r_m,r_f,sampl)=Inf;
num_inc_pair_powmin_FBS_sample(clstr_m,clstr_f,r_m,r_f,sampl)=Inf;
p_JSPA_PM_sample{clstr_m,clstr_f,r_m,r_f,sampl}=[];
alpha_JSPA_PM_sample{clstr_m,clstr_f,r_m,r_f,sampl}=[];
SIC_order_JSPA_PM_sample{clstr_m,clstr_f,r_m,r_f,sampl}=[];
P_tot_JSPA_PM_sample(clstr_m,clstr_f,r_m,r_f,sampl)=-1e+10;
alpha_initial=zeros(1,B); %alpha_initial=0 (Tightenning the Lower Bound)
[alpha,p,P_tot_iter,iteration_num,SIC_order,h_norm,Feasible_JSPA,diverges]=...
    func_alg_minimize_pow(B,U,M,h,h_cell,N0,R_min,P_max,ICI_indctr,eps_tol,alpha_initial);
Feasible_JSPA_sample(clstr_m,clstr_f,r_m,r_f,sampl)=Feasible_JSPA;
diverge_JSPA_samples(clstr_m,clstr_f,r_m,r_f,sampl)=diverges;
if diverges==0
    p_JSPA_PM=p;
    p_JSPA_PM_sample{clstr_m,clstr_f,r_m,r_f,sampl}=p;
    alpha_JSPA_PM=alpha;
    alpha_JSPA_PM_sample{clstr_m,clstr_f,r_m,r_f,sampl}=alpha;
    SIC_order_JSPA_PM=SIC_order;
    SIC_order_JSPA_PM_sample{clstr_m,clstr_f,r_m,r_f,sampl}=SIC_order;
    P_tot_JSPA_PM_sample(clstr_m,clstr_f,r_m,r_f,sampl)=P_tot_iter(end,end);
    lambda=[];
    [lambda]=func_Lambda(B,U,M,SIC_order);
    for b=1:B
        if U(b)>=2
            tot_pairs(b)=U(b)*(U(b)-1)/2; %number of total pairs
        else tot_pairs(b)=1;
        end
        num_inc_pair_powmin(b)=sum(nonzeros(lambda_CNR(b,:,:)~=lambda(b,:,:)),'all')./2; %number of incorrect orders
    end
    num_inc_pair_powmin_MBS_sample(clstr_m,clstr_f,r_m,r_f,sampl)=num_inc_pair_powmin(1);
    num_inc_pair_powmin_FBS_sample(clstr_m,clstr_f,r_m,r_f,sampl)=num_inc_pair_powmin(2);
end

%% Power Minimization Problem: Optimal joint rate and power allocation (JRPA) with CNR-based decoding order 
Feasible_JRPA_sample(clstr_m,clstr_f,r_m,r_f,sampl)=0;
diverge_JRPA_samples(clstr_m,clstr_f,r_m,r_f,sampl)=1;
p_JRPA_PM_sample{clstr_m,clstr_f,r_m,r_f,sampl}=[];
alpha_JRPA_PM_sample{clstr_m,clstr_f,r_m,r_f,sampl}=[];
P_tot_JRPA_PM_sample(clstr_m,clstr_f,r_m,r_f,sampl)=-1e+10;
if diverge_JSPA_samples(clstr_m,clstr_f,r_m,r_f,sampl)==0
    if min(SIC_order_JSPA_PM==SIC_CNR,[],'all')==1
        Feasible_JRPA_sample(clstr_m,clstr_f,r_m,r_f,sampl)=Feasible_JSPA_sample(clstr_m,clstr_f,r_m,r_f,sampl);
        diverge_JRPA_samples(clstr_m,clstr_f,r_m,r_f,sampl)=diverge_JSPA_samples(clstr_m,clstr_f,r_m,r_f,sampl);
        p_JRPA_PM_sample{clstr_m,clstr_f,r_m,r_f,sampl}=p_JSPA_PM_sample{clstr_m,clstr_f,r_m,r_f,sampl};
        alpha_JRPA_PM_sample{clstr_m,clstr_f,r_m,r_f,sampl}=alpha_JSPA_PM_sample{clstr_m,clstr_f,r_m,r_f,sampl};
        P_tot_JRPA_PM_sample(clstr_m,clstr_f,r_m,r_f,sampl)=P_tot_JSPA_PM_sample(clstr_m,clstr_f,r_m,r_f,sampl);
    else %Solving...
        SIC_order=SIC_CNR; %CNR-based decoding order 
        lambda=lambda_CNR; %cancellation decision indicator according to the CNR-based decoding order 
        p=[]; ICI=[];
        cvx_begin quiet
        cvx_precision best
            variable p(B,M)
            subject to
    %         %power constraint
    %         sum(p,2)<=P_max';
            %Min rate constraint (main)
            ICI=reshape(sum(repmat(sum(p,2),1,B,M).*ICI_indctr.*h,1),B,M); %Inter-cell Interference
            for b=1:B
                for m=1:U(b)
                    1e+15*p(b,m).*h_cell(b,m) >= 1e+15*(2.^R_min(b,m) - 1) .* (sum(reshape(lambda(b,m,:),1,M).*p(b,:)) .* h_cell(b,m) ...
                        + ICI(b,m) + N0(b,m));
                end
            end    
            %SIC constraint
            for b=1:B
                for i=1:U(b)
                    for k=1:U(b)
                        if lambda(b,i,k)==1
                            1e+15*p(b,i).*h_cell(b,k) >= 1e+15*(2.^R_min(b,i) - 1) .* (sum(reshape(lambda(b,i,:),1,M).*p(b,:)) .* h_cell(b,k) ...
                                + ICI(b,k) + N0(b,k));
                        end
                    end
                end
            end
            %Positive power
            p>=0;
            minimize sum(sum(p))   
        cvx_end
        if min(isfinite(p),[],'all')==1
            diverge_JRPA_samples(clstr_m,clstr_f,r_m,r_f,sampl)=0;
            p_JRPA_PM_sample{clstr_m,clstr_f,r_m,r_f,sampl}=p;
            alpha_JRPA_PM_sample{clstr_m,clstr_f,r_m,r_f,sampl}=sum(p,2)'./P_max;
            P_tot_JRPA_PM_sample(clstr_m,clstr_f,r_m,r_f,sampl)=sum(p,'all');
            Feasible_JRPA_sample(clstr_m,clstr_f,r_m,r_f,sampl)=...
                (max(alpha_JRPA_PM_sample{clstr_m,clstr_f,r_m,r_f,sampl})<=1);
        else
            p=[];  ICI=[];
        end
    end
end

%% Power Minimization Problem: CNR-based decoding order with fixed rate power allocation (FRPA)
Feasible_FRPA_sample(clstr_m,clstr_f,r_m,r_f,sampl)=0;
diverge_FRPA_samples(clstr_m,clstr_f,r_m,r_f,sampl)=1;
p_FRPA_PM_sample{clstr_m,clstr_f,r_m,r_f,sampl}=[];
alpha_FRPA_PM_sample{clstr_m,clstr_f,r_m,r_f,sampl}=[];
P_tot_FRPA_PM_sample(clstr_m,clstr_f,r_m,r_f,sampl)=-1e+10;
if diverge_JSPA_samples(clstr_m,clstr_f,r_m,r_f,sampl)==0
    if min(SIC_order_JSPA_PM==SIC_CNR,[],'all')==1
        Feasible_FRPA_sample(clstr_m,clstr_f,r_m,r_f,sampl)=Feasible_JSPA_sample(clstr_m,clstr_f,r_m,r_f,sampl);
        diverge_FRPA_samples(clstr_m,clstr_f,r_m,r_f,sampl)=diverge_JSPA_samples(clstr_m,clstr_f,r_m,r_f,sampl);
        p_FRPA_PM_sample{clstr_m,clstr_f,r_m,r_f,sampl}=p_JSPA_PM_sample{clstr_m,clstr_f,r_m,r_f,sampl};
        alpha_FRPA_PM_sample{clstr_m,clstr_f,r_m,r_f,sampl}=alpha_JSPA_PM_sample{clstr_m,clstr_f,r_m,r_f,sampl};
        P_tot_FRPA_PM_sample(clstr_m,clstr_f,r_m,r_f,sampl)=P_tot_JSPA_PM_sample(clstr_m,clstr_f,r_m,r_f,sampl);
    else 
        SIC_order=SIC_CNR; %SIC ordering based on CNR
        lambda=lambda_CNR; %cancellation decision indicator based on CNR-Based Decoding Order
        p=[]; ICI=[];
        cvx_begin quiet
        cvx_precision best
            variable p(B,M)
            subject to
    %         %power constraint
    %         sum(p,2)<=P_max';
            %Min rate constraint
            ICI=reshape(sum(repmat(sum(p,2),1,B,M).*ICI_indctr.*h,1),B,M); %Inter-cell Interference
            for b=1:B
                for m=1:U(b)
                    1e+15*p(b,m).*h_cell(b,m) >= 1e+15*(2.^R_min(b,m) - 1) .* (sum(reshape(lambda(b,m,:),1,M).*p(b,:)).*h_cell(b,m) ...
                        + ICI(b,m) + N0(b,m));
                end
            end
            %SIC constraint
            for b=1:B
                for i=1:U(b)
                    for k=1:U(b)
                        if lambda(b,i,k)==1
                            1e+24* h_cell(b,k) * ( ICI(b,i) + N0(b,i) ) >= 1e+24* h_cell(b,i) * ( ICI(b,k) + N0(b,k) );
                        end
                    end
                end
            end
            %Positive power
            p>=0;
            minimize sum(sum(p))   
        cvx_end
        if min(isfinite(p),[],'all')==1
            diverge_FRPA_samples(clstr_m,clstr_f,r_m,r_f,sampl)=0;
            p_FRPA_PM_sample{clstr_m,clstr_f,r_m,r_f,sampl}=p;
            alpha_FRPA_PM_sample{clstr_m,clstr_f,r_m,r_f,sampl}=sum(p,2)'./P_max;
            P_tot_FRPA_PM_sample(clstr_m,clstr_f,r_m,r_f,sampl)=sum(p,'all');
            Feasible_FRPA_sample(clstr_m,clstr_f,r_m,r_f,sampl)=...
                (max(alpha_FRPA_PM_sample{clstr_m,clstr_f,r_m,r_f,sampl})<=1);
        else
            p=[];  ICI=[];
        end
    end
end
%%
tt=tt+1;
if (tt/tot_samplss)==0.25
    fprintf('*** 25 percentage of samples are calculated! ***\n\n')
elseif (tt/tot_samplss)==0.5
    fprintf('*** 50 percentage of samples are calculated! ***\n\n')
elseif (tt/tot_samplss)==0.75
    fprintf('*** 75 percentage of samples are calculated! ***\n\n')
end
                end
            end
        end
    end
end

fprintf('*** Finished! ***\n\n')

save('STEP2_1_Alg_Powmin_usernum','P_max','rate_min_m','rate_min_f','eps_tol',...
    'SIC_order_CNR_sample','lambda_CNR_sample',...
    'num_inc_pair_powmin_MBS_sample','num_inc_pair_powmin_FBS_sample',...
    'p_JSPA_PM_sample','alpha_JSPA_PM_sample','SIC_order_JSPA_PM_sample',...
    'P_tot_JSPA_PM_sample','Feasible_JSPA_sample','diverge_JSPA_samples',...
    'Feasible_JRPA_sample','diverge_JRPA_samples','p_JRPA_PM_sample',...
    'alpha_JRPA_PM_sample','P_tot_JRPA_PM_sample',...
    'Feasible_FRPA_sample','diverge_FRPA_samples','p_FRPA_PM_sample',...
    'alpha_FRPA_PM_sample','P_tot_FRPA_PM_sample')