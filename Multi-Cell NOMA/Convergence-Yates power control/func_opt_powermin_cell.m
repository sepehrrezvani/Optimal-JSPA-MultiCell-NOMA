function [p_cell]=func_opt_powermin_cell(b,U,M,SIC_order,h_norm,R_min)
T=(2.^R_min-1)./h_norm;
p_cell=zeros(1,M);
    cnter=0;
    order_user=SIC_order;
    while (cnter <= U(b)-1)
        str_usr=find(order_user(b,1:U(b))==max(order_user(b,1:U(b))));
        p_cell(1,str_usr)=max((2.^R_min(b,str_usr)-1).*(  (1./h_norm(b,str_usr)) + sum(p_cell(1,:))),0);
        order_user(b,str_usr)=-1;
        cnter=cnter+1;
    end

