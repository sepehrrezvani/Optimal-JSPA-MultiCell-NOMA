clear all
clc
%% Simulation Settings
%%%%%Network Topology
%BS placement
Dis_BS=200; %BS to center
Cor_BS=func_CoordinateBS(Dis_BS); %Coordinate of BSs
B=numel(Cor_BS); %Number of BSs
%User placement
radius_BS=[500,40*ones(1,B-1)]; %radius of each BS in meter
%Number of users in each cell
Macro_cell_users=3;
Femto_cell_users=2;
U=[Macro_cell_users,Femto_cell_users*ones(1,B-1)];
M=max(U);
min_dis_user=[20,2*ones(1,B-1)]; %minimum distance of user to associated BS in meter
%Noise power
BW=5e+6; %wireless bandwidth
PSD_N0=-174; %PSD of Noise in dBm
PSD_N0=10.^((PSD_N0-30)/10); %PSD of Noise in watts
N0=abs(randn(B,M)).*PSD_N0.*BW; %AWGN power at users
%%%%%Coordinate of users
Cor_user=func_CoordinateUser(B,Cor_BS,U,M,radius_BS,min_dis_user);
[h,h_cell]=func_ChannelGain(B,U,M,Cor_BS,Cor_user);
%%Interfering BS indicator for simplifying the simulation codes
%%Interfering BS indicator for simplifying the simulation codes
ICI_indctr=func_ICI_indicator(B,M);
%%%%%Transmit power of BSs
P_MBS=46; %MBS power in dbm
P_FBS=30; %MBS power in dbm
P_max=[P_MBS,P_FBS*ones(1,B-1)]; %BSs power (in dbm)
P_max=10.^((P_max-30)./10); %BSs power in Watts
%%%%%Minimum spectral efficiency
R_macro=1;%bps/Hz
R_femto=1;%bps/Hz
[R_min]=func_minrate(B,U,M,R_macro,R_femto);

%% Feasibility Check in ICI-free Scenario>> Are There Enough Powers to Maintain Rate Demands?!
[Feasible_free,SIC_CNR,lambda_CNR,alpha_free]=func_ICI_free_scenario(h_cell,N0,B,U,M,R_min,P_max);

%% Total Power Minimization Problem: Optimal joint power allocation and SIC ordering (JSPA) >>> Convergence
eps_tol=0; %stopping criterion
%%%%%alpha=0
fprintf('***Power Minimization Problem: Optimal JSPA (Convergence):***\n\n\n')
alpha_initial=zeros(1,B); %alpha_initial=0 (Tightenning the Lower Bound)
[alpha,p,P_tot_iter,iteration_num,SIC_order,h_norm,Feasible_JSPA,diverges]=...
    func_alg_minimize_pow(B,U,M,h,h_cell,N0,R_min,P_max,ICI_indctr,eps_tol,alpha_initial);
P_tot_zero=reshape(P_tot_iter',1,numel(P_tot_iter))

%%%%%alpha=1
alpha_initial=ones(1,B); %alpha_initial=1 (Tightenning the Upper Bound)
[alpha,p,P_tot_iter,iteration_num,SIC_order,h_norm,Feasible_JSPA,diverges]=...
    func_alg_minimize_pow(B,U,M,h,h_cell,N0,R_min,P_max,ICI_indctr,eps_tol,alpha_initial);
P_tot_one=reshape(P_tot_iter',1,numel(P_tot_iter))

save('Result_Converged_JSPA','Dis_BS','radius_BS','Cor_BS','Cor_user',...
    'B','U','M','h','h_cell','N0','R_min','P_max','ICI_indctr',...
    'eps_tol','P_tot_zero','P_tot_one')

%% Power Minimization Problem: Optimal decoding order >>>Divergence
fprintf('***Power Minimization Problem: Optimal JSPA (Divergence):***\n\n\n')
%%%%%Minimum spectral efficiency
R_macro=4;%bps/Hz
R_femto=4;%bps/Hz
[R_min]=func_minrate(B,U,M,R_macro,R_femto);
eps_tol=0; %stopping criterion

%%%%%alpha=0
alpha_initial=zeros(1,B); %alpha_initial=0 (Tightenning the Lower Bound)
[alpha,p,P_tot_iter,iteration_num,SIC_order,h_norm,Feasible_JSPA,diverges]=...
    func_alg_minimize_pow(B,U,M,h,h_cell,N0,R_min,P_max,ICI_indctr,eps_tol,alpha_initial);
P_tot_zero_DV=reshape(P_tot_iter',1,numel(P_tot_iter))

%%%%%alpha=1
alpha_initial=ones(1,B); %alpha_initial=1 (Tightenning the Upper Bound)
[alpha,p,P_tot_iter,iteration_num,SIC_order,h_norm,Feasible_JSPA,diverges]=...
    func_alg_minimize_pow(B,U,M,h,h_cell,N0,R_min,P_max,ICI_indctr,eps_tol,alpha_initial);
P_tot_one_DV=reshape(P_tot_iter',1,numel(P_tot_iter))

save('Result_Divergence_JSPA','Dis_BS','radius_BS','Cor_BS','Cor_user',...
    'B','U','M','h','h_cell','N0','R_min','P_max','ICI_indctr',...
    'eps_tol','P_tot_zero_DV','P_tot_one_DV')

%% Figure: Network Topology
Fig_topology_Yate=figure;
hold on
t=0:0.1:6.3;

Leg_coverage_MBS=plot(radius_BS(1).*sin(t)+real(Cor_BS(1)),radius_BS(1).*cos(t)+imag(Cor_BS(1)),'LineStyle','-.','Color',[0 0 0],'linewidth',2);axis equal
Leg_coverage_FBS=plot(radius_BS(2).*sin(t)+real(Cor_BS(2)),radius_BS(2).*cos(t)+imag(Cor_BS(2)),'LineStyle','--','Color',[0 0 0.5],'linewidth',0.01);axis equal

%Location of BSs
Leg_MBS=plot(real(Cor_BS(1)),imag(Cor_BS(1)),'MarkerFaceColor',[0 0 0],'MarkerEdgeColor','none',...
    'MarkerSize',10,'Marker','d','LineStyle','none','Color',[0 0 0]);
Leg_FBS=plot(real(Cor_BS(2)),imag(Cor_BS(2)),'MarkerFaceColor',[0 0 0],'MarkerEdgeColor','none',...
    'MarkerSize',8,'Marker','o','LineStyle','none','Color',[0 0 0]);

%Location of MBSusers
for b=1:1
    for m=1:U(b)
        Leg_MBSuser=plot(real(Cor_user(b,m)),imag(Cor_user(b,m)),'MarkerFaceColor',[0 0 0],...
            'MarkerEdgeColor',[0 0 0],...
            'MarkerSize',10,...
            'Marker','x',...
            'LineWidth',2,...
            'LineStyle','none',...
            'Color',[0 0 0]);
    end
end
%Location of FBSusers
for b=2:2
    for m=1:U(b)
        Leg_FBSuser=plot(real(Cor_user(b,m)),imag(Cor_user(b,m)),'MarkerFaceColor',[0 0.447058826684952 0.74117648601532],...
            'MarkerEdgeColor',[0 0.447058826684952 0.74117648601532],...
            'MarkerSize',7,...
            'Marker','*',...
            'LineWidth',1,...
            'LineStyle','none',...
            'Color',[0 0 0]);
    end
end
xlabel('Horizontal axis coordinate (m)','FontSize',14,...
    'FontName','Times New Roman')
ylabel('Vertical axis coordinate (m)','FontSize',14,...
    'FontName','Times New Roman')
% Create legend
legend1 = legend([Leg_coverage_MBS,Leg_coverage_FBS,Leg_MBS,Leg_FBS,Leg_MBSuser,Leg_FBSuser],...
    'Coverage area of MBS','Coverage area of FBS','MBS','FBS','Macro-cell user','Femto-cell user');
set(legend1,'FontSize',12,'FontName','Times New Roman','EdgeColor',[0 0 0])
set(gca,'XTick',[-500:200:500],'YTick',[-500:100:500],'FontName','Times New Roman','FontSize',14)
grid on
savefig(Fig_topology_Yate,'Fig_topology_Yate')

%% Plot Figure Convergence
clear all
clc
load('Result_Converged_JSPA','P_tot_zero','P_tot_one')
P_tot_zero=10*log10(P_tot_zero'); %watts to dBm
P_tot_one=10*log10(P_tot_one'); %watts to dBm
iter_num=min(numel(P_tot_zero),numel(P_tot_one));

Fig_convergence_Yate=figure;
hold on
plot([1:iter_num],P_tot_one(1:iter_num),'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','>','linewidth',1,'Color','k','linestyle','-');
plot([1:iter_num],P_tot_zero(1:iter_num),'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','o','linewidth',1,'Color','k','linestyle','-');

legend('\alpha^{(1)}_b=1','\alpha^{(1)}_b=0',...
       'location','best','NumColumns',1);
set(legend,'FontSize',14,'FontName','Times New Roman','EdgeColor',[0 0 0])     
xlabel('Iteration index','FontSize',16,...
    'FontName','Times New Roman')
ylabel('Total power consumption (dBm)','FontSize',16,...
    'FontName','Times New Roman')
set(gca,'FontName','Times New Roman','FontSize',14)
grid on
savefig(Fig_convergence_Yate,'Fig_convergence_Yate')

%% Plot Figure Divergence
clear all
clc
load('Result_Divergence_JSPA','P_tot_zero_DV','P_tot_one_DV')
P_tot_zero=10*log10(P_tot_zero_DV'); %watts to dBm
P_tot_one=10*log10(P_tot_one_DV'); %watts to dBm
iter_num=min(numel(P_tot_zero),numel(P_tot_one));

Fig_Divergence_Yate=figure;
hold on
plot([1:iter_num],P_tot_one(1:iter_num),'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','>','linewidth',1,'Color','k','linestyle','-');
plot([1:iter_num],P_tot_zero(1:iter_num),'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','o','linewidth',1,'Color','k','linestyle','-');

legend('\alpha^{(1)}_b=1','\alpha^{(1)}_b=0',...
       'location','best','NumColumns',1);
set(legend,'FontSize',14,'FontName','Times New Roman','EdgeColor',[0 0 0])     
xlabel('Iteration index','FontSize',16,...
    'FontName','Times New Roman')
ylabel('Total power consumption (dBm)','FontSize',16,...
    'FontName','Times New Roman')
set(gca,'FontName','Times New Roman','FontSize',14)
grid on
savefig(Fig_Divergence_Yate,'Fig_Divergence_Yate')
