function [Feasible_free,SIC_CSI,lambda_CSI,alpha_free]=func_ICI_free_scenario(h_cell,N0,B,U,M,R_min,P_max)
ICI=zeros(B,M);
h_norm=h_cell./(ICI+N0);
[SIC_order]=func_SICordering(B,U,M,h_norm);
SIC_CSI=SIC_order; %CSI based SIC ordering is optimal in ICI-free Scenario
[lambda]=func_Lambda(B,U,M,SIC_order);%cancellation decision indicator
lambda_CSI=lambda;
[p]=func_opt_powermin(B,U,M,SIC_order,h_norm,R_min);
alpha_free=sum(p,2)'./P_max;
if (max(alpha_free)>1)
    Feasible_free =0;
    fprintf('***ICI-free Scenario: Infeasible >>> Not Enough Power!!***\n\n\n')
else Feasible_free =1;
    fprintf('***ICI-free Scenario: Feasible***\n\n\n')
end
