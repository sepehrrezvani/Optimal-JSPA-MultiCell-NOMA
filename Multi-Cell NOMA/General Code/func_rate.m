function [r]=func_rate(B,U,M,SIC_order,p,h_norm)
[lambda]=func_Lambda(B,U,M,SIC_order);%cancellation decision indicator
% %%%%%%%%%%%%lambda_CSI(b,m,m_prn)=1; signal of user m_prn is treated as noise at user m
r=zeros(B,M);
INI=zeros(B,M);
for b=1:B
    for m=1:U(b)
        INI(b,m)=sum(reshape(lambda(b,m,:),1,M).*p(b,:));
    end
    r(b,:)=log2( 1 + (p(b,:).*h_norm(b,:))./(1+INI(b,:).*h_norm(b,:)) );
end

