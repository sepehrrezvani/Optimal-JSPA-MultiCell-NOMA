function [p]=func_optpower_sumrate(B,U,M,h_norm,R_minextra,SIC_order,P_max,alpha)
p=zeros(B,M);
T=(2.^R_minextra-1)./h_norm;
for b=1:B
    cnter=0;
    order_user=SIC_order;
    while (cnter <= U(b)-2)
        weak_usr=find(order_user(b,1:U(b))==min(order_user(b,1:U(b))));
        p(b,weak_usr)=max(T(b,weak_usr).*(  1 + (alpha(b)*P_max(b)-sum(p(b,:)))*h_norm(b,weak_usr)  )/(2.^R_minextra(b,weak_usr)),0);
        order_user(b,weak_usr)=M+1;
        cnter=cnter+1;
    end
    srng_usr=find(order_user(b,1:U(b))==min(order_user(b,1:U(b))));
    p(b,srng_usr)=max(alpha(b)*P_max(b) - sum(p(b,:)),0);
end
