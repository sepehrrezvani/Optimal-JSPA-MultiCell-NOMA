function [p]=func_opt_powermin(B,U,M,SIC_order,h_norm,R_min)
T=(2.^R_min-1)./h_norm;
p=zeros(B,M);
for b=1:B
    cnter=0;
    order_user=SIC_order;
    while (cnter <= U(b)-1)
        str_usr=find(order_user(b,1:U(b))==max(order_user(b,1:U(b))));
        p(b,str_usr)=max(T(b,str_usr).*(  1 + sum(p(b,:))*h_norm(b,str_usr)  ),0);
        order_user(b,str_usr)=-1;
        cnter=cnter+1;
    end
end
