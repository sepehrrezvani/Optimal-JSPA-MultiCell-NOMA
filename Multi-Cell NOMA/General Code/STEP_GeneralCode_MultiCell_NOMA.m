clear all
clc
%% Simulation Settings
%%%%%Network Topology
%BS placement
Dis_BS=200; %BS to center
Cor_BS=func_CoordinateBS(Dis_BS); %Coordinate of BSs
B=numel(Cor_BS); %Number of BSs
%User placement
radius_BS=[500,40*ones(1,B-1)]; %radius of each BS in meter
%Number of users in each cell
Macro_cell_users=3;
Femto_cell_users=2;
U=[Macro_cell_users,Femto_cell_users*ones(1,B-1)];
M=max(U);
min_dis_user=[20,2*ones(1,B-1)]; %minimum distance of user to associated BS in meter
%Noise power
BW=5e+6; %wireless bandwidth
PSD_N0=-174; %PSD of Noise in dBm/Hz
PSD_N0=10.^((PSD_N0-30)/10); %PSD of Noise in watts
N0=abs(randn(B,M)).*PSD_N0.*BW; %AWGN power at users
%%%%%Coordinate of users
Cor_user=func_CoordinateUser(B,Cor_BS,U,M,radius_BS,min_dis_user);
[h,h_cell]=func_ChannelGain(B,U,M,Cor_BS,Cor_user);
%%Interfering BS indicator for simplifying the simulation codes
ICI_indctr=func_ICI_indicator(B,M);
%%%%%Transmit power of BSs
P_MBS=46; %MBS power in dbm
P_FBS=30; %MBS power in dbm
P_max=[P_MBS,P_FBS*ones(1,B-1)]; %BSs power (in dbm)
P_max=10.^((P_max-30)./10); %BSs power in Watts
%%%%%Minimum spectral efficiency
R_macro=1;%bps/Hz
R_femto=1.3;%bps/Hz
[R_min]=func_minrate(B,U,M,R_macro,R_femto);

%% Figure of Network Topology
[Leg_coverage_BS]=func_FigTopology(B,U,radius_BS,Cor_BS,Cor_user);
clear Dis_BS Cor_BS radius_BS min_Dis_User Cor_user Leg_coverage_BS b F_cell ...
  M_cell R_macro R_femto Femto_cell Macro_cell mean_N0 P_MBS P_FBS

%% Feasibility Check in ICI-free Scenario >> Are there enough powers to maintain rate demands?!
[Feasible_free,SIC_CNR,lambda_CNR,alpha_free]=func_ICI_free_scenario(h_cell,N0,B,U,M,R_min,P_max);

%% Total Power Minimization Problem: Optimal joint power allocation and SIC ordering (JSPA)
fprintf('***Power Minimization Problem: Optimal JSPA:***\n\n\n')
eps_tol=1e-6; %stopping criterion
alpha_initial=zeros(1,B); %alpha_initial=0 (Tightenning the Lower Bound)
[alpha,p,P_tot_iter,iteration_num,SIC_order,h_norm,Feasible_JSPA,diverges]=...
    func_alg_minimize_pow(B,U,M,h,h_cell,N0,R_min,P_max,ICI_indctr,eps_tol,alpha_initial);
alpha_opt_PM=alpha; %optimal alpha
p_opt_PM=p; %optimal power
P_tot_opt_PM=P_tot_iter(end,end); %total power at the converged point
SIC_order_opt_PM=SIC_order; % optimal decoding order (at the converged point)
if diverges==1
    fprintf('---Diverged >>> Infeasible without power constraint---\n\n')
elseif diverges==0 && Feasible_JSPA==0
    fprintf('---Converged >>> but infeasible (Not Enough Power!)---\n\n')
    fprintf('Minimum alpha:\n\n')
    disp(alpha)
elseif Feasible_JSPA==1
    fprintf('---Feasible!---\n\n')
%%%%%Results
fprintf('Total power consumption (mW):\n')
disp(P_tot_opt_PM*1e+3)
fprintf('Optimal alpha:\n')
disp(alpha_opt_PM)
fprintf('Optimal powers (in percentage of total power):\n')
disp(100*p_opt_PM./repmat(P_max',1,M))
fprintf('Optimal SIC ordering:\n')
disp(SIC_order_opt_PM)
fprintf('Total power consumption at each iteration (mW):\n')
disp(P_tot_iter*1e+3)
fprintf('Spectral efficiency of users (bps/Hz):\n')
[r]=func_rate(B,U,M,SIC_order,p,h_norm);
disp(r)
end

%% Power Minimization Problem: Optimal joint rate and power allocation (JRPA) with CNR-based decoding order 
Feasible_JRPA=0;
if (diverges==0)
    fprintf('\n***Power Minimization Problem: CNR-based decoding order with Optimal JRPA***\n\n\n')
    %Solving...
    h_norm=h_cell./(N0); %normalized channel gains by noise (within the cells)
    SIC_order=SIC_CNR; %CNR-based decoding order 
    lambda=lambda_CNR; %cancellation decision indicator according to the CNR-based decoding order 
    p=[]; ICI=[];
    cvx_begin quiet
    cvx_precision best
        variable p(B,M)
        subject to
%         %power constraint
%         sum(p,2)<=P_max';
        %Min rate constraint (main)
        ICI=reshape(sum(repmat(sum(p,2),1,B,M).*ICI_indctr.*h,1),B,M); %Inter-cell Interference
        for b=1:B
            for m=1:U(b)
                1e+15*p(b,m).*h_cell(b,m) >= 1e+15*(2.^R_min(b,m) - 1) .* (sum(reshape(lambda(b,m,:),1,M).*p(b,:)) .* h_cell(b,m) ...
                    + ICI(b,m) + N0(b,m));
            end
        end    
        %SIC constraint
        for b=1:B
            for i=1:U(b)
                for k=1:U(b)
                    if lambda(b,i,k)==1
                        1e+15*p(b,i).*h_cell(b,k) >= 1e+15*(2.^R_min(b,i) - 1) .* (sum(reshape(lambda(b,i,:),1,M).*p(b,:)) .* h_cell(b,k) ...
                            + ICI(b,k) + N0(b,k));
                    end
                end
            end
        end
        %Positive power
        p>=0;
        minimize sum(sum(p))   
    cvx_end
    if min(isfinite(p),[],'all')==1
        p_opt_JRPA_PM=p;
        alpha_JRPA_PM=sum(p,2)'./P_max; %optimal alpha
        Feasible_JRPA=(max(alpha_JRPA_PM)<=1);
        P_tot_JRPA_PM=sum(p,'all');
        P_tot_gap_JRPA=100*(P_tot_JRPA_PM-P_tot_opt_PM)./P_tot_opt_PM;
        if Feasible_JRPA==1
            fprintf('---Feasible!---\n\n')
        else fprintf('---Infeasible! Not enough power!---\n\n')
        end
        fprintf('Total power consumption (mW):\n')
        disp(P_tot_JRPA_PM.*1e+3)
        fprintf('Optimal alpha:\n')
        disp(alpha_JRPA_PM)
        fprintf('Performance (optimality) gap (in percentage):\n')
        disp(P_tot_gap_JRPA)
    else fprintf('---Outage is occurred without power constraint\n\n')
    end
end

%% Power Minimization Problem: CNR-based decoding order with fixed-rate power allocation (FRPA)...
    %%>>> Includes SIC necessary condition
Feasible_FRPA=0;
if (diverges==0)
    fprintf('\n***Power Minimization Problem: CNR-based decoding order with Optimal FRPA***\n\n\n')
    fprintf('Optimal SIC decoding order\n')
    disp(SIC_order_opt_PM)
    fprintf('CNR-based decoding order:\n')
    disp(SIC_CNR)
    fprintf('SIC ordering differences matrix between optimal and heuristic:\n')
    disp(SIC_order_opt_PM~=SIC_CNR)
    fprintf('Percentage of user pairs with different decoding order in each cell:\n')
    SIC_order=SIC_order_opt_PM;
    [lambda]=func_Lambda(B,U,M,SIC_order);
    lambda_opt=lambda;
    for b=1:B
        if U(b)>=2
            tot_pairs(b)=U(b)*(U(b)-1)/2; %number of total pairs
        else tot_pairs(b)=1;
        end
        num_inc_pair_powmin(b)=sum(nonzeros(lambda_CNR(b,:,:)~=lambda_opt(b,:,:)),'all')./2; %number of incorrect orders
        perc_inc_order_powmin(b)=100*(num_inc_pair_powmin(b))./tot_pairs(b);
    end 
    disp(perc_inc_order_powmin)
    
    %Solving...
    h_norm=h_cell./(N0); %normalizing the channel gains by noise
    SIC_order=SIC_CNR; %SIC ordering based on CNR
    lambda=lambda_CNR; %cancellation decision indicator based on CNR-Based Decoding Order
    clear p
    clear ICI
    cvx_begin quiet
    cvx_precision best
        variable p(B,M)
        subject to
%         %power constraint
%         sum(p,2)<=P_max';
        %Min rate constraint
        ICI=reshape(sum(repmat(sum(p,2),1,B,M).*ICI_indctr.*h,1),B,M); %Inter-cell Interference
        for b=1:B
            for m=1:U(b)
                1e+15*p(b,m).*h_cell(b,m) >= 1e+15*(2.^R_min(b,m) - 1) .* (sum(reshape(lambda(b,m,:),1,M).*p(b,:)).*h_cell(b,m) ...
                    + ICI(b,m) + N0(b,m));
            end
        end
        %SIC constraint
        for b=1:B
            for i=1:U(b)
                for k=1:U(b)
                    if lambda(b,i,k)==1
                        1e+24* h_cell(b,k) * ( ICI(b,i) + N0(b,i) ) >= 1e+24* h_cell(b,i) * ( ICI(b,k) + N0(b,k) );
                    end
                end
            end
        end
        %Positive power
        p>=0;
        minimize sum(sum(p))   
    cvx_end
    if min(isfinite(p),[],'all')==1
        alpha_FRPA_PM=sum(p,2)'./P_max; %optimal alpha
        Feasible_FRPA=(max(alpha_FRPA_PM)<=1);
        P_tot_FRPA_PM=sum(p,'all');
        P_tot_gap_FRPA=100*(P_tot_FRPA_PM-P_tot_opt_PM)./P_tot_opt_PM;
        if Feasible_FRPA==1
            fprintf('---Feasible!---\n\n')
        else fprintf('---Infeasible! Not enough power!---\n\n')
        end
        fprintf('Total power consumption (mW):\n')
        disp(P_tot_FRPA_PM.*1e+3)
        fprintf('Optimal alpha:\n')
        disp(alpha_FRPA_PM)
        fprintf('Performance (optimality) gap (in percentage):\n')
        disp(P_tot_gap_FRPA)
    else fprintf('Outage is occurred without power constraint\n\n')
    end
end

%% Sum-rate Maximization Problem: Optimal JSPA
clear p alpha ICI
eps_a=1e-2; %stepsize alpha
if (Feasible_JSPA==1)
    fprintf('***Sum-rate Maximization Problem: Optimal JSPA\n\n\n');
    R_opt=0;
    while R_opt<=0
        %statistics for computation time
        alpha_min=alpha_opt_PM;
        ceil_amin=ceil(alpha_min*ceil(inv(eps_a)))./inv(eps_a); %vector of minimum alpha vector
        S_a=round((1-ceil_amin).*inv(eps_a)); %vector of number of samples for alpha
        X = sprintf('Please wait about %d seconds!\n',round(prod(S_a)/12600,1));...
        disp(X)
        fprintf('Number of samples in vector alpha:')
        disp(S_a)
        X = sprintf('Total number of samples for finding alpha: %d\n',prod(S_a));
        disp(X)
        [p_opt,r_opt,alpha_opt,SIC_order_opt,R_opt]=...
            func_alg_maximize_sumrate(B,U,M,R_min,P_max,ceil_amin,ICI_indctr,h,h_cell,N0,eps_a);
        p_opt_sumrate=p_opt;
        r_opt_sumrate=r_opt;
        alpha_opt_sumrate=alpha_opt;
        SIC_order_opt_sumrate=SIC_order_opt;
        R_opt_sumrate=R_opt;
        if R_opt<=0 
            if eps_a>1e-3
                fprintf('---Feasible solution is not found! Reduce eps_a!\n\n')
                fprintf('---------New stepsize for alpha!\n\n')
                eps_a=eps_a./2;
                disp(eps_a); %stepsize alpha
            else
                fprintf('---Feasible solution is not found for (eps_a=0.001)!\n\n')
                break
            end
        end
    end
    if R_opt>0
    %%%%%Results
        fprintf('Optimal SIC ordering:\n')
        disp(SIC_order_opt_sumrate)
        fprintf('Spectral efficiency of users (bps/Hz):\n')
        disp(r_opt_sumrate)
        fprintf('Total spectral efficiency of users (bps/Hz):\n')
        disp(R_opt_sumrate)
        fprintf('Optimal alpha:\n')
        disp(alpha_opt_sumrate)
        fprintf('Optimal powers (in percentage of alpha*P_max):\n')
        p_frac=100*p_opt_sumrate./repmat(alpha_opt_sumrate'.*P_max',1,M);
        disp(p_frac)
        %Performance of approximated closed-form optimal powers
        fprintf('Approximated powers (in percentage of alpha*P_max):\n')
        SIC_order=SIC_order_opt;
        [q]=func_approx_sumratemax(B,U,M,SIC_order,R_min);
        disp(q)
        fprintf('Approximation gap (in percentage): users allocated power gaps:\n')
        approx_gap_users=zeros(B,M);
        for b=1:B
            approx_gap_users(b,1:U(b))=100*abs((p_frac(b,1:U(b))-q(b,1:U(b)))./p_frac(b,1:U(b)));
        end
        disp(approx_gap_users)
        fprintf('Approximation gap (in percentage): Total spectral efficiency gap:\n')
        p=q.*repmat((alpha_opt_sumrate.*P_max)',1,M)/100;
        SIC_order=SIC_order_opt_sumrate;
        ICI=reshape(sum(repmat(alpha_opt_sumrate'.*P_max',1,B,M).*ICI_indctr.*h,1),B,M);
        h_norm=h_cell./(ICI+N0);
        [r]=func_rate(B,U,M,SIC_order,p,h_norm);
        R_tot_approx=sum(r,'all');
        disp(100*abs((R_opt_sumrate-R_tot_approx)./R_tot_approx))
    end
end

%% Sum-rate Maximization Problem: CNR-Based Decoding Order with Suboptimal JRPA
if (Feasible_JRPA==1)
    fprintf('\n***Sum-rate Maximization Problem: CNR-Based decoding order with JRPA***\n\n\n')
    %Initialization step
    h_norm=h_cell./(N0); %CNR-Based SIC
    SIC_order=SIC_CNR; %SIC ordering based on CNR
    lambda=lambda_CNR; %cancellation decision indicator
    m=(log(2.^30 - 1) - log(2.^(29.999) - 1))/0.001; %approx term log(2.^r - 1);
    p=[]; p_tilde=[]; r=[]; ICI=[];
    t=1; %iteration index
    cvx_begin quiet
    cvx_precision default
        variable p_tilde(B,M)
        variable r(B,M)
        subject to
        %power constraint
        sum(exp(p_tilde),2)<=P_max';
        %Min rate constraint (main)
        ICI=reshape(sum(repmat(sum(exp(p_tilde),2),1,B,M).*ICI_indctr.*h,1),B,M); %Inter-cell Interference
        for b=1:B
            for i=1:M
                if i<=U(b)
                    1e2*(p_tilde(b,i) + log(h_cell(b,i))) >= 1e2*( m*r(b,i) + ...
                        log( sum(reshape(lambda(b,i,:),1,M).* exp(p_tilde(b,:)) ) .* h_cell(b,i) ...
                        + ICI(b,i) + N0(b,i)));
                else r(b,i)==0
                end
            end
        end    
        %SIC constraint
        for b=1:B
            for i=1:U(b)
                for k=1:U(b)
                    if lambda(b,i,k)==1
                        1e2*(p_tilde(b,i) + log(h_cell(b,k))) >= 1e2*( m*r(b,i) + ...
                            log( sum(reshape(lambda(b,i,:),1,M).* exp(p_tilde(b,:)) ) .* h_cell(b,k) ...
                            + ICI(b,k) + N0(b,k)));
                    end
                end
            end
        end
        %minimum rate
        r>=R_min;
        %Positive power
        r>=0;
        maximize sum(sum(r))   
    cvx_end
    if min(isfinite(r),[],'all')==0
        fprintf('\n---Initial "r" is infeasible---\n\n')
        r=R_min; p=p_opt_JRPA_PM; p_tilde=log(p_opt_JRPA_PM); ICI=[]; % In this case, we use minimum rate equality initialization
    end
    R_user_iter_JRPA{t}=r;
    R_tot_iter_JRPA(t)=sum(r,'all');
    p_iter_JRPA{t}=exp(p_tilde);
    % Sequential Programming
    r_old=0;
    eps_s=1e-1;
    while (norm(r-r_old) > eps_s)
        t=t+1;
        r_old=r;    p_tilde_old=p_tilde;
        %CVX
        r=[];   p=[];   p_tilde=[]; ICI=[];
        cvx_begin quiet
        cvx_precision default
            variable p_tilde(B,M)
            variable r(B,M)
            subject to
            %power constraint
            sum(exp(p_tilde),2)<=P_max';
            %Min rate constraint (main)
            ICI=reshape(sum(repmat(sum(exp(p_tilde),2),1,B,M).*ICI_indctr.*h,1),B,M); %Inter-cell Interference
            for b=1:B
                for i=1:M
                    if i<=U(b)
                        1e2*(p_tilde(b,i) + log(h_cell(b,i))) >= 1e2*( log(2^r_old(b,i)-1) + ...
                            (2^r_old(b,i)/(2^r_old(b,i)-1))*(r(b,i)-r_old(b,i)) + ...
                            log( sum(reshape(lambda(b,i,:),1,M).* exp(p_tilde(b,:)) ) .* h_cell(b,i) ...
                            + ICI(b,i) + N0(b,i)));
                    else r(b,i)==0
                    end
                end
            end
            %SIC constraint
            for b=1:B
                for i=1:U(b)
                    for k=1:U(b)
                        if lambda(b,i,k)==1
                            1e2*(p_tilde(b,i) + log(h_cell(b,k))) >= 1e2*( log(2.^r_old(b,i)-1) + ...
                            ((2.^r_old(b,i))/(2.^r_old(b,i)-1))*(r(b,i)-r_old(b,i)) + ...
                            log( sum(reshape(lambda(b,i,:),1,M).* exp(p_tilde(b,:)) ) .* h_cell(b,k) ...
                            + ICI(b,k) + N0(b,k)));
                        end
                    end
                end
            end
            %minimum rate
            r>=R_min;
            %Positive power
            r>=0;
            maximize sum(sum(r))   
        cvx_end
        if min(isfinite(r),[],'all')==0
            fprintf('\n---CVX failed to solve the problem!---\n\n')
            r=r_old;    p_tilde=p_tilde_old;    p=[];   ICI=[];
            break
        end
        R_user_iter_JRPA{t}=r;
        R_tot_iter_JRPA(t)=sum(r,'all');
        p_iter_JRPA{t}=exp(p_tilde);
    end
    p=exp(p_tilde);
    %Results
    fprintf('Optimal SIC ordering:\n')
    disp(SIC_order_opt_sumrate)
    fprintf('CNR-Based SIC Ordering:\n')
    disp(SIC_CNR)
    fprintf('alpha:\n')
    alpha_JRPA_SM=sum(p,2)'./P_max;
    disp(alpha_JRPA_SM)
    fprintf('Optimal powers (in percentage of alpha*P_max):\n')
    p_frac_JRPA_SM=100*p./repmat(alpha_JRPA_SM'.*P_max',1,M);
    disp(p_frac_JRPA_SM)
    fprintf('Spectral efficiency of users (bps/Hz):\n')
    disp(r)
    fprintf('Total spectral efficiency of users (bps/Hz):\n')
    disp(sum(r,'all'))
    fprintf('Total spectral efficiency (optimality) gap in percentage:\n')
    disp(100*(R_opt_sumrate-sum(r,'all'))/sum(r,'all'))
    fprintf('Total spectral efficiency of users (bps/Hz) through iterations:\n')
    disp(R_tot_iter_JRPA)
end

%% Sum-rate Maximization Problem: CNR-Based Decoding Order with Optimal FRPA
clear p alpha ICI
eps_a=1e-2; %stepsize alpha
R_opt=0;
if (Feasible_FRPA==1)
    fprintf('\n***Sum-rate Maximization Problem: CNR-Based Decoding Order with FRPA:\n\n\n')
    feasible_SIC=0;
    while R_opt<=0
        %statistics for computation time
        alpha_min=alpha_opt_PM;
        ceil_amin=ceil(alpha_min*ceil(inv(eps_a)))./inv(eps_a); %vector of minimum alpha vector
        S_a=round((1-ceil_amin).*inv(eps_a)); %vector of number of samples for alpha
        X = sprintf('Please wait about %d seconds!\n',round(prod(S_a)/12600,1));...
        disp(X)
        fprintf('Number of samples in vector alpha:\n')
        disp(S_a)
        X = sprintf('Total number of samples for finding alpha: %d\n',prod(S_a));
        disp(X)
        [p_opt,r_opt,alpha_opt,R_opt,feasible_SIC]=...
            func_alg_PA_sumrate(B,U,M,R_min,P_max,ceil_amin,ICI_indctr,h,h_cell,N0,eps_a,SIC_CNR);
        p_FRPA_sumrate=p_opt;
        r_FRPA_sumrate=r_opt;
        alpha_FRPA_sumrate=alpha_opt;
        R_FRPA_sumrate=R_opt;
        if feasible_SIC==0 
            if eps_a>1e-3
                fprintf('---Feasible solution is not found! Reduce eps_a!\n\n')
                fprintf('---------New stepsize for alpha!\n\n')
                eps_a=eps_a./2;
                disp(eps_a); %stepsize alpha
            else
                fprintf('---Feasible solution is not found for (eps_a=0.001)!\n\n')
                break
            end
        end
    end
    if feasible_SIC==1
        %%Results
        fprintf('Spectral efficiency of users (bps/Hz):\n')
        disp(r_FRPA_sumrate)
        fprintf('Total spectral efficiency of users (bps/Hz):\n')
        disp(R_FRPA_sumrate)
        fprintf('Optimal alpha:\n')
        disp(alpha_FRPA_sumrate)
        X = sprintf('Performance gap between decoding orders (in percentage): %d\n',100*(R_opt_sumrate-R_FRPA_sumrate)./R_FRPA_sumrate);
            disp(X)
        fprintf('Optimal SIC ordering:\n')
        disp(SIC_order_opt_sumrate)
        fprintf('CNR-based SIC ordering:\n')
        disp(SIC_CNR)
        fprintf('SIC ordering differences matrix between optimal and heuristic:\n')
        disp(SIC_order_opt_sumrate~=SIC_CNR)
        fprintf('Percentage of user pairs with incorrect decoding order in each cell:\n')
        SIC_order=SIC_order_opt_sumrate;
        [lambda]=func_Lambda(B,U,M,SIC_order);
        lambda_opt=lambda;
        for b=1:B
            if U(b)>=2
                tot_pairs(b)=U(b)*(U(b)-1)/2; %number of total pairs
            else tot_pairs(b)=1;
            end
            num_inc_pair_sumrate(b)=sum(nonzeros(lambda_CNR(b,:,:)~=lambda_opt(b,:,:)),'all')./2; %number of incorrect orders
            perc_inc_order_sumrate(b)=100*(num_inc_pair_sumrate(b))./tot_pairs(b);
        end 
        disp(perc_inc_order_sumrate)
    end
end

%% Sum-rate Maximization Problem: Fully Distributed Framework
if (Feasible_JSPA==1)
    fprintf('\n***Sum-rate Maximization Problem: Fully Distributed Framework***\n\n')
    alpha=ones(1,B);
    ICI=reshape(sum(repmat(alpha'.*P_max',1,B,M).*ICI_indctr.*h,1),B,M);
    h_norm=h_cell./(ICI+N0);
    [SIC_order]=func_SICordering(B,U,M,h_norm);
    for b=1:B
        for i=1:U(b)
            R_minextra(b,i)=R_min(b,i)+0.0000001;
        end
    end
    [p]=func_optpower_sumrate(B,U,M,h_norm,R_minextra,SIC_order,P_max,alpha);
    [r]=func_rate(B,U,M,SIC_order,p,h_norm);
    if (min(min(r>=R_min))==1)
        R_tot_Dist=sum(r,'all');
        fprintf('Spectral efficiency of users (bps/Hz):\n')
        disp(r)
        fprintf('Total spectral efficiency of users (bps/Hz):\n')
        disp(R_tot_Dist)
        X = sprintf('Performance gap between decoding orders (in percentage): %d\n',100*(R_opt_sumrate-R_tot_Dist)./R_tot_Dist);
        disp(X)
    else R_tot_Dist=0;
        fprintf('Outage is occurred!\n\n')
    end
end

%% Sum-rate Maximization Problem: Semi-Centralized Algorithm
clear p alpha ICI
if (Feasible_JSPA==1)
    fprintf('\n***Sum-rate Maximization Problem: Semi-Centralized Algorithm\n\n\n');
    %statistics for computation time
    eps_a=1e-3; %stepsize alpha
    alpha_min=alpha_opt_PM;
    ceil_amin=ceil(alpha_min*ceil(inv(eps_a)))./inv(eps_a); %vector of minimum alpha vector
    S_a=round((1-ceil_amin).*inv(eps_a)); %vector of number of samples for alpha
    X = sprintf('Please wait about %d seconds!\n',round(S_a(1)/12600,1));...
    disp(X)
    fprintf('Number of samples for alpha_MBS:')
    disp(S_a(1))
    [p_SC,r_SC,alpha_SC,SIC_order_SC,Rtot_SC,Feasible_SC]=...
        func_alg_SC_sumrate(B,U,M,R_min,P_max,ceil_amin,ICI_indctr,h,h_cell,N0,eps_a);
    %%%%%Results
    if (max(max(SIC_order_SC))>1)
        fprintf('Spectral efficiency of users (bps/Hz):\n')
        disp(r_SC)
        fprintf('Total spectral efficiency of users (bps/Hz):\n')
        disp(Rtot_SC)
        X = sprintf('Performance gap between decoding orders (in percentage): %d\n',100*(R_opt_sumrate-Rtot_SC)./Rtot_SC);
        disp(X)
    else fprintf('Outage is occured\n\n')
    end
end

%% Sufficient Condition Examination
fprintf('\n***SIC sufficient condition for verifying the optimality of the CNR-based decoding order\n\n\n')
[perc_pair_unsatisfy,num_pairs_unsatisfy]=func_suff_condition_optSIC(B,U,M,h,h_cell,P_max,N0);
fprintf('Total number of user pairs in each cell:\n\n')
%Total number of user pairs in each cell: M!/(M-2)!2!
for b=1:B
    Tot_pairs(b)=U(b)*(U(b)-1)/2;
end
disp(Tot_pairs)
fprintf('Total number of user pairs violating the sufficient condition in each cell:\n\n')
disp(num_pairs_unsatisfy)
fprintf('Percentage of user pairs which cannot satisfy the sufficient condition:\n\n')
disp(perc_pair_unsatisfy)
