function [p_hr_sumrate,r_hr_sumrate,alpha_hr_sumrate,R_hr_sumrate]=...
    func_PA_heuristicorder_sumrate(B,U,M,R_min,P_max,ICI_indctr,h,h_cell,N0,SIC_CSI,alpha_opt_powmin,R_opt_sumrate)
fprintf('***Optimal power allocation based on the prefixed (heuristic) SIC ordering\n\n\n')
%statistics for computation time
eps_a=1e-2; %stepsize alpha
alpha_min=alpha_opt_powmin;
ceil_amin=ceil(alpha_min*ceil(inv(eps_a)))./inv(eps_a); %vector of minimum alpha vector
S_a=round((1-ceil_amin).*inv(eps_a)); %vector of number of samples for alpha
X = sprintf('Please wait about %d seconds!\n\n',round(prod(S_a)/12600));...
disp(X)
fprintf('Number of samples in vector alpha:')
disp(S_a)
X = sprintf('Total number of Samples for finding alpha: %d\n',prod(S_a));
disp(X)
[p_opt,r_opt,alpha_opt,R_opt,feasible_SIC]=...
    func_alg_PA_sumrate(B,U,M,R_min,P_max,ceil_amin,ICI_indctr,h,h_cell,N0,eps_a,SIC_CSI);
p_hr_sumrate=p_opt;
r_hr_sumrate=r_opt;
alpha_hr_sumrate=alpha_opt;
R_hr_sumrate=R_opt;

if feasible_SIC==0
    fprintf('-----Unsuccessful SIC! >>> Outage is occurred\n\n')
else %%Results
    fprintf('Results:\n\n\n')
    fprintf('Spectral efficiency of users (bps/Hz):\n')
    disp(r_hr_sumrate)
    fprintf('Total spectral efficiency of users (bps/Hz):\n')
    disp(R_hr_sumrate)
    fprintf('Optimal alpha (fraction of maximum power):\n')
    disp(alpha_hr_sumrate)
    X = sprintf('Performance gap between decoding orders (in percentage): %d\n\n\n',round(100*(R_opt_sumrate-R_hr_sumrate)./R_hr_sumrate,1));
        disp(X)
end