clear all
clc
r=[0.001:0.001:15];
y=log(2.^r - 1);
% figure
% 
% plot(r,y,'linewidth',2,'Color','k');
% xlabel('r','FontSize',16,...
%     'FontName','Times New Roman')
% ylabel('y','FontSize',16,...
%     'FontName','Times New Roman')
% grid on

% figure
% 
% diff_y=(2.^r)./(2.^r-1);
% plot(r,diff_y,'linewidth',2,'Color','k');
% xlabel('r','FontSize',16,...
%     'FontName','Times New Roman')
% ylabel('first derivative of y','FontSize',16,...
%     'FontName','Times New Roman')
% grid on

%approx 1
y0=y(1);
m=(y(length(y))-y(length(y)-1))./(r(length(r))-r(length(y)-1));
y_aprx=m*r;
y_aprx2=log(2.^r);
Fig_approxrate=figure;
hold on
plot(r,y_aprx,'linestyle','-','linewidth',2,'Color','r');
plot(r,y,'linewidth',2,'linestyle','-.','Color','k');
% plot(r,y_aprx2,'linewidth',1,'Color','b');
legend('y=m.r','y=ln(2^r-1)','location','north','NumColumns',1);
set(legend,'FontSize',12,'FontName','Times New Roman','EdgeColor',[0 0 0])
xlabel('r','FontSize',16,...
    'FontName','Times New Roman')
ylabel('y','FontSize',16,...
    'FontName','Times New Roman')
set(gca,'XTick',[0:15],'YTick',[-10:2:10],'FontName','Times New Roman','FontSize',14)
grid on
savefig(Fig_approxrate,'Fig_approxrate')
