function [Cor_user] = func_CoordinateUser(B,Cor_BS,U,M,radius_BS,min_Dis_User)
%Maco-cell users
Coordinate=0;
Mokh_user=0;
t=0;
while 6>0 
    Dis_user_BS=min_Dis_User(1)+(radius_BS(1)-min_Dis_User(1))*unifrnd(0,1);
    angle_user = 2*pi*unifrnd(0,1);
    crd_user=Dis_user_BS.*( cos(angle_user) + i*sin(angle_user) );
    if ( min(abs(crd_user - Cor_BS(1,2:B)) > radius_BS(1,2:B))==1 )
        t=t+1;
        Mokh_user=[Mokh_user,crd_user];
        Mokh_user(1)=[]; %Coordinated of users in each cell
        Coordinate=[Coordinate,Mokh_user];
    end
    if (t==U(1))
        break
    end
end
%Femto-cell users
for b=2:B
    Mokh_user=0;
    t=0;
    while 6>0 
        Dis_user_BS=min_Dis_User(b)+(radius_BS(b)-min_Dis_User(b))*unifrnd(0,1);
        angle_user = 2*pi*unifrnd(0,1);
        crd_user=Dis_user_BS.*( cos(angle_user) + i*sin(angle_user) );
        crd_user=crd_user+Cor_BS(b);
        t=t+1;
        Mokh_user=[Mokh_user,crd_user];
        if (t==U(b))
            break
        end
    end
    Mokh_user(1)=[]; %Coordinated of users in each cell
    Coordinate=[Coordinate,Mokh_user];
end
Coordinate(1)=[]; %Coordinated of users in cells
Cor_user=zeros(B,M);
Cor_user(1,1:U(1))=Coordinate(1,1:U(1));
for b=2:B
    Cor_user(b,1:U(b))=Coordinate(1,sum(U(1,1:b-1))+1:sum(U(1,1:b)));
end



